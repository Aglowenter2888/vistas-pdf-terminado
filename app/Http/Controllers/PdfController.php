<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
class PdfController extends Controller
{     
    //Metodos PDF CapturaExp 
        public function pdfForm()
        {
            return view('expedientes.create');
        }
     
        public function pdfDownload(Request $request){
     
           request()->validate([
            'Titulo' => 'required',
            'Asunto' => 'required',
            'Date' => 'required',
            'Descripcion' => 'required',
            'Docente' => 'required',
            'Area' => 'required'
            ]);
          
             $data = 
             [
                'Titulo' => $request->Titulo,
                'Asunto' => $request->Asunto,
                'Date' => $request->Date,
                'Descripcion' => $request->Descripcion,
                'Docente' => $request->Docente,
                'Area' => $request->Area
             ];
           $pdf = PDF::loadView('pdf_download', $data);
       
           return $pdf->stream('informe.pdf');
        }

        //PDF Gestion Academica
        public function pdfFormGestion()
        {
            return view('users.Gestioncademica');
        }

        public function pdfDownload_GestionA(Request $request){
            request()->validate([
                'Titulo' => 'required',
                'Date' => 'required',
                'DateF' => 'required',
                'Area' => 'required'
                ]);
              
                 $data = 
                 [
                    'Titulo' => $request->Titulo,
                    'Date' => $request->Date,
                    'DateF' => $request->DateF,
                    'Area' => $request->Area
                 ];
               $pdf = PDF::loadView('pdf_download_GestionA', $data);
           
               return $pdf->stream('informeGestion.pdf');
        }

                //PDF Productos Desarrollados
                public function pdfFormProductos()
                {
                    return view('users.Productos_desarrollados');
                }
        
                public function pdfDownload_Productos(Request $request){
                    request()->validate([
                        'NombreArticulo' => 'required',
                        'NombreInforme' => 'required',
                        'Proyecto' => 'required',
                        'Patente' => 'required',
                        'Libro' => 'required',
                        'Capitulo' => 'required',
                        'Date' => 'required',
                        'Area' => 'required'
                        ]);
                      
                         $data = 
                         [
                            'NombreArticulo' => $request->NombreArticulo,
                            'NombreInforme' => $request->NombreInforme,
                            'Proyecto' => $request->Proyecto,
                            'Patente' => $request->Patente,
                            'Libro' => $request->Libro,
                            'Capitulo' => $request->Capitulo,
                            'Date' => $request->Date,
                            'Area' => $request->Area
                         ];
                       $pdf = PDF::loadView('pdf_download_ProductosD', $data);
                   
                       return $pdf->stream('informeGestion.pdf');
                }
       
     
    }

