@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')

    <!-- Bootstrap CSS -->
<div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Crear nuevo documento</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">					

	    <!-- Contenido de  perfil -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                    </head>
  <body>
      <table class="table table-striped">
          <tr>
              <th>#</th>
              <th>Titulo</th>
              <th>Autor</th>
              <th>Descripcion</th>
              <th>Ver</th>
              <th>Descargar</th>
              <th>Eliminar</th>
            </tr>
                @foreach ($file as $key=>$data)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$data->titulo}}</td>
                    <td>{{$data->autor}}</td>
                    <td>{{$data->descripcion}}</td>
                    <td><a class="btn btn-success" href="/files/{{$data->id}}">ver</a></td>
                    <td><a class="btn btn-success" href="/file/download/{{$data->file}}">Descargar</a> </td>
                    <td>
                        <form action="{{route("archivos.destroy", $data->id)}}" method="POST"> 
                        @method("DELETE")
                        @csrf
                       <input type="submit" class="btn btn-danger" value="Eliminar">
                       </form>
                    </td>
                    </tr>
                @endforeach
      </table>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
                     
<!-- Fin de formulario de gestion academida -->





@endsection