@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')
  <!-- Bootstrap CSS -->
  <div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Crear nuevo documento</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">	
<!---->
<!doctype html>
<html lang="en">
  <head>
    <title>Document</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Crear un nuevo Documento</div>
                    </div>
                <div class="card-body">
                    <form>
                        <div class="form-row">
      <form action="/files" method="POST" enctype="multipart/form-data">

        @csrf 
        <div class="form-group col-md-6 ">
        <label for="inputEmail4">Nombre Del Titulo:</label>
        <input type="text" name="titulo" placeholder="titulo">
        </div>

        <div class="form-group col-md-6 ">
        <label for="inputEmail4">Nombre Del Autor:</label>
        <input type="text" name="autor" placeholder="autor">
        </div>

        <div class="form-group col-md-6 ">
        <label for="inputEmail4">Agregar Descripcion:</label>
        <input type="text" name="descripcion" placeholder="descripcion">
      

          <input type="file" class="btn btn-light" name="file">
          <br>
          <input type="submit" class="btn btn-primary" value="Enviar">  <br>
      </form>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

                          
                                                              
                  
             

@endsection